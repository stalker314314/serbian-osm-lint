# -*- coding: utf-8 -*-

import re
import overpy

p_point = re.compile('Point\((?P<lat>[-0-9.]+)\s(?P<lon>[-0-9.]+)\)')
p_url = re.compile('https://www.openstreetmap.org/(?P<type>.*)/(?P<id>\d+)')


class OsmLintEntity(object):
    """
    Since our entities can either be of various types (PyOsmium, osmread...), this is wrapper to abstract those types.
    """

    def __init__(self, entity, is_osmium):
        if isinstance(entity, dict) and not is_osmium:
            self._convert_from_sophox(entity)
            return

        if isinstance(entity, overpy.Node) or isinstance(entity, overpy.Way) or isinstance(entity, overpy.Relation):
            self._convert_from_overpass(entity)
            return

        self.id = entity.id
        self.lat, self.lon = 0, 0
        self.origin = 'pbf'

        self.entity_type = self._get_entity_type(entity, is_osmium)

        if isinstance(entity.tags, dict):
            self.tags = entity.tags
            self.lat = entity.lat
            self.lon = entity.lon

        else:
            if self.entity_type == 'node':
                self.lat = entity.location.lat
                self.lon = entity.location.lon
            self.tags = {}
            for tag in entity.tags:
                self.tags[tag.k] = tag.v

    def _convert_from_sophox(self, entity):
        url = entity['id']['value']
        m = p_url.match(url)
        if not m:
            raise Exception('Unexpected URL for entity. It was {}', url)
        self.id = int(m.group('id'))
        self.entity_type = m.group('type')

        loc = entity['loc']['value']
        m = p_point.match(loc)
        if not m:
            raise Exception('Invalid format for point. Expected Point(lat lon) and got {}', loc)
        self.lat = float(m.group('lat'))
        self.lon = float(m.group('lon'))
        self.origin = 'sophox'
        self.tags = {}
        for key in entity:
            if key in ('id', 'loc'):  # Skip these special ones
                continue
            if key == 'metadata':  # Special key holding our metadata definition from start of the query
                self.tags[key] = entity[key]
            else:
                self.tags[key] = entity[key]['value']

    def _convert_from_overpass(self, entity):
        self.id = entity.id
        self.origin = 'overpass'
        if isinstance(entity, overpy.Node):
            self.entity_type = 'node'
            self.lat = float(entity.lat)
            self.lon = float(entity.lon)
        elif isinstance(entity, overpy.Way):
            self.entity_type = 'way'
            self.lat = entity.center_lat
            self.lon = entity.center_lon
        elif isinstance(entity, overpy.Relation):
            self.entity_type = 'relation'
            self.lat = entity.center_lat
            self.lon = entity.center_lon
        self.tags = {}
        for tag in entity.tags:
            self.tags[tag] = entity.tags[tag]

    @staticmethod
    def _get_entity_type(entity, is_osmium):
        """
        Helper method to get type of the entity.
        :param entity: Entity to get type from
        :param is_osmium: Whether entity is coming from Osmium library
        :return: Type of the entity. Can be one of the 'node', 'way', 'relation'
        """
        if is_osmium:
            import osmium
            if isinstance(entity, osmium.osm.Node):
                return 'node'
            elif isinstance(entity, osmium.osm.Way):
                return 'way'
            elif isinstance(entity, osmium.osm.Relation):
                return 'relation'

        try:
            import osmread
            if isinstance(entity, osmread.Node):
                return 'node'
            elif isinstance(entity, osmread.Way):
                return 'way'
            elif isinstance(entity, osmread.Relation):
                return 'relation'
        except ImportError:
            pass

        raise Exception('Entity is neither PyOsmium not osmread known type'.format(entity))
