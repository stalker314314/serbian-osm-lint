# -*- coding: utf-8 -*-

import unittest
from transliteration import cyr2lat, lat2cyr


class TestTransliteration(unittest.TestCase):

    def test_cyr2lat(self):
        self.assertEqual(cyr2lat(" !@#$%^&*()"), " !@#$%^&*()")
        self.assertEqual(cyr2lat("foo"), "foo")
        self.assertEqual(cyr2lat("FOO"), "FOO")
        self.assertEqual(cyr2lat("GHФу"), "GHFu")
        self.assertEqual(cyr2lat("Asd Čč Ljlj Ćć Njnj Šš Žž Dždž"), "Asd Čč Ljlj Ćć Njnj Šš Žž Dždž")
        self.assertEqual(cyr2lat("Чачак Љиг Ћићевац Његошева Шабац Пољана Мањеж Жагубица Џивџан"),
                         "Čačak Ljig Ćićevac Njegoševa Šabac Poljana Manjež Žagubica Dživdžan")

    def test_city_node(self):
        self.assertEqual(lat2cyr(" !@#$%^&*()"), " !@#$%^&*()")
        self.assertEqual(lat2cyr("test TEST"), "тест ТЕСТ")
        self.assertEqual(lat2cyr("LJa NJe DŽu"), "Ља Ње Џу")
        self.assertEqual(lat2cyr("Čačak Ljig Ćićevac Njegoševa Šabac Poljana Manjež Žagubica Dživdžan"),
                         "Чачак Љиг Ћићевац Његошева Шабац Пољана Мањеж Жагубица Џивџан")

if __name__ == '__main__':
    unittest.main()
